export const state = () => ({
	searching: false,
	timer: false
})

export const actions = {
	toggleSearch(context, payload) {
		context.commit('setSearch', payload)
	},
	toggleTimer(context, payload) {
		context.commit('setTimer', payload)
	}
}

export const mutations = {
	setSearch(state, payload) {
		state.searching = payload
	},
	setTimer(state, payload) {
		state.timer = payload
	}
}
