import { shallowMount } from '@vue/test-utils'
import Lobby from '~/components/Lobby.vue'

const factory = () => {
	return shallowMount(Lobby, {})
}

describe('Lobby', () => {
	test('mounts properly', () => {
		const wrapper = factory()
		expect(wrapper.isVueInstance()).toBeTruthy()
	})

	test('renders properly', () => {
		const wrapper = factory()
		expect(wrapper.html()).toMatchSnapshot()
	})
})
