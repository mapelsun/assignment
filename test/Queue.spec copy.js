import { shallowMount } from '@vue/test-utils'
import Queue from '~/pages/queue.vue'

const factory = () => {
	return shallowMount(Queue, {})
}

describe('Queue', () => {
	test('mounts properly', () => {
		const wrapper = factory()
		expect(wrapper.isVueInstance()).toBeTruthy()
	})

	test('renders properly', () => {
		const wrapper = factory()
		expect(wrapper.html()).toMatchSnapshot()
	})
})
